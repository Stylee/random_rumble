import { HIT_MONSTER, HIT_BACK } from "../constants/action-types";

const initialState = {
    players: {
	1: { name: "John", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 1 },
	2: { name: "Jack", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 2 },
	3: { name: "Jessy", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 3 },
	4: { name: "Jenny", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 4 }
    },
    monster: {
        pv: 800,
        pvMax: 800,
        bgType: 'bg-danger',
        faType: 'fa-heart',
        barName: ' : pv'
    }
};

function rootReducer(state = initialState, action) {

    if (action.type === HIT_MONSTER) {
	return {
            ...state,
	    monster : {
		...state.monster,
                pv: state.monster.pv - action.payload
            }
	};

	// Object.assign({}, state, {
	//     monster: state.monster.concat(action.payload)
	// });
	// }
    }

    if (action.type === HIT_BACK) {
	/* console.log('state', state) */
	console.log('payload', action.payload.player);

	const player = action.payload.player;
	
     	return {
            ...state,
            players: {
		...state.players,
		[action.payload.player]: {
		    ...state.players[action.payload.player],
                    pv: state.players[action.payload.player].pv - action.payload.force
		}
            }
	};
    }

    return state;
};

export default rootReducer;
